// single room (insertOne method)

db.users.insertOne({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	roomsAvailable: 10,
	isAvailable: false

});


// multiple rooms (insertMany method) 

db.users.insertMany([
	{
	name: "double",
	accomodates: 3.0,
	price: 2000.0,
	description: "A room fit for a small family going on a vacation",
	roomsAvailable: 5.0,
	isAvailable: false
    },
    {
	name: "queen",
	accomodates: 4.0,
	price: 4000.0,
	description: "A room with a queen sized bed perfect for a simple getaway",
	roomsAvailable: 15,
	isAvailable: false
    }
]);


// find method

db.users.find({name: "double"});

// updateOne method


db.users.updateOne({
	name: "queen"
},
	{
	$set: 
	{
	roomsAvailable: 0,
	}
});


// deleteMany method 

db.users.deleteMany({
	roomsAvailable: 0
});


